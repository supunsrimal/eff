   
   <?php wp_footer(); ?>

  <footer>

    <div id="footer-section" class="container-fluid footer-section">

      <div class="row">
        
        <div class="col-sm-12 social-icons">
          
          <ul>
            
            <li class="fb">

              <a href="https://www.facebook.com/Esufally-Family-Foundation-141902516530651/" target="_blank"> <!-- <img src="<?php //bloginfo('stylesheet_directory'); ?>/images/fb.png" alt=""> --> </a>

            </li>

            <li class="ytube">

              <a href="https://www.youtube.com/channel/UC_GvaHGpIa_Zt-IuTHkC77w" target="_blank"> <!-- <img src="<?php //bloginfo('stylesheet_directory'); ?>/images/ytube.png" alt=""> --> </a>

            </li>

<!--             <li class="twitter">
              
              <a href="#" target="_blank">  </a>

            </li> -->

            <li class="instagram">
              
              <a href="https://www.instagram.com/effsrilanka/" target="_blank"> <!-- <img src="<?php //bloginfo('stylesheet_directory'); ?>/images/instagram.png" alt=""> --> </a>

            </li>

          </ul>

        </div>

        <div class="col-sm-12 contact-details">
          
          <ul>
            
            <li> <p> CONTACT US <span class="arrow">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; > </span> </p> </li>

            <li> <p> Esufally Family Foundation, 9th Floor, 75 Baybrooke Place, Colombo 02. </p> </li>

            <li> <p> E: <a href="mailto:avanti.mn@gmail.com">avanti.mn@gmail.com</a> </p> </li>

            <li> <p> M: +94773055287 </p> </li>

          </ul>

        </div>

        <div class="col-sm-12 copy-right-sec">
          
          <p> Copyright 2017&nbsp;·&nbsp;All rights reserved. </p>

        </div>

        <div class="col-sm-12 wad-logo">

          <a href="http://wearedesigners.net/" target="_blank">
            
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/wad-logo.png" alt="">

          </a>
          
        </div>

      </div>

    </div>

   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script>
    
    <!-- dynamics JS for menu animation -->
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/dynamics.min.js"></script>

    <!-- Swiper JS -->
    <script src="<?php bloginfo('stylesheet_directory'); ?>/swiper-slider/js/swiper.min.js"></script>

  </footer>

    <script>

      $(".dropdown-menu li").removeClass('main-tab');

      var $sideNav = $('#side-nav')[0];
      var $sideNavBtns = $('#menu-main_menu li.main-tab');
      var clciked = 0;

      $('#nav-toggle').on('click',function(){
        if ($('body').hasClass('nav-open')) {
          $('body').removeClass('nav-open');
          dynamics.animate($sideNav, {
            translateX: 0
          }, {
            duration: 200,
            delay: 150
          });
          for(var i=0; i<$sideNavBtns.length; i++) {
            dynamics.animate($sideNavBtns[i], {
              translateX: 0
            }, {
              type: dynamics.spring,
              duration: 2000,
              friction: 400,
              delay: i * 50
            })
          }
        } else {

          $('body').addClass('nav-open');
          dynamics.animate($sideNav, {
            translateX: 500
          }, {
            type: dynamics.spring,
            duration: 800
          });
          for(var i=0; i<$sideNavBtns.length; i++) {
            dynamics.animate($sideNavBtns[i], {
              translateX: 300
            }, {
              type: dynamics.spring,
              duration: 1000,
              friction: 400,
              delay: (i * 150) + 200
            })
          }
        }

        

        if(clciked == 0){
            $('#nav-toggle').animate(
              {
                'left' : '268px'
              },100);
          clciked = 1;
        }
        else{
          if(window.matchMedia( "(max-width: 320px)" ).matches || window.matchMedia( "(max-width: 480px)" ).matches ){
            $('#nav-toggle').animate(
            {
              'left' : '0px'
            },1000);
          clciked = 0;
          }
          else{
          $('#nav-toggle').animate(
            {
              'left' : '50px'
            },1000);
          clciked = 0;
          }
        }

      });
    </script>


  