<?php
/**
 * Template Name: about us
 *
 **/

get_header(); 

	while ( have_posts() ) : the_post(); 

		//get page default values 

		$page_banner_image = get_field('page_banner_image');

		$banner_caption = get_field('banner_caption');

		$banner_content = get_field('banner_content');

		$history_section_content = get_field('history_section_content');

		$our_grant_making_section_content = get_field('our_grant_making_section_content');

		$how_we_work_section_content = get_field('how_we_work_section_content');

	endwhile; // End of the loop.

?>

	<div class="about-us-page">
		
		<div class="container-fluid banner-sec">
			
			<div class="row">
				
				<div class="col-sm-12 logo">

					<a href="<?php echo site_url(); ?>">

						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-2.png" alt="">
						
					</a>

				</div>

				<div class="col-sm-12 banner no-padding">
					<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
					
					<img class="main-imgs" src="<?php echo $page_banner_image; ?>" alt="">

					<div class="banner-caption">

						<h1> <?php echo $banner_caption; ?> </h1>

						<p> <?php echo $banner_content; ?> </p>

					</div>

				</div>

			</div>

		</div>

		<div class="container-fluid section-2">
			
			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> THE ESUFALLY FAMILY FOUNDATION </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 no-padding">
					
					<div class="col-sm-6">
						
						<div class="content-1">
							
							<h2> HISTORY </h2>

							<?php echo $history_section_content; ?>

						</div>

					</div>

					<div class="col-sm-6 sec-img no-padding">
						
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/abt-us-sec-1.jpg" alt="">

					</div>

				</div>

			</div>

		</div>

		<div id="how_work" class="container-fluid section-3">
			
			<div class="row">
				
				<div class="col-sm-4 sec-img no-padding">
					
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/abt-us-sec-2.jpg" alt="">

				</div>

				<div class="col-sm-8 content-sec">
					
					<div class="col-sm-12 content-1">
						
						<h2> Our Grantmaking </h2>

						<?php echo $our_grant_making_section_content; ?>

					</div>

					<div class="col-sm-12 content-2">
						
						<h2> How We Work </h2>

						<?php echo $how_we_work_section_content; ?>

					</div>

				</div>

			</div>

		</div>

		<div id="our_core_val" class="container-fluid section-4">
			
			<div class="row content-wrapper">
				
				<div class="col-sm-12 title">
					
					<h1>OUR CORE VALUES</h1>

				</div>

				<div class="details-sec">

					<?php 

					    //wordpress query to get posts from post type abt_us_our_core_valu
					                
					    $args = array('posts_per_page' => 100,'post_type' => 'abt_us_our_core_valu');

					    $the_query_main = new WP_Query($args);

					    while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

					    $our_core_values_sec_titles = get_the_title();

					    $our_core_values_sec_content = get_the_content();

					?>
					
						<div class="col-sm-4 details">
							
							<h3> <?php echo $our_core_values_sec_titles; ?> </h3>

							<hr>

							<p> <?php echo $our_core_values_sec_content; ?> </p>

							<hr>

						</div>

					<?php endwhile; //end loop ?>

				</div>

			</div>

		</div>

	</div>


 <?php get_footer(); ?>