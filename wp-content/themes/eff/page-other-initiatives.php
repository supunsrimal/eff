<?php
/**
 * Template Name: other initiatives
 *
 **/

	get_header(); 

	while ( have_posts() ) : the_post(); 

		$page_banner_image = get_field('page_banner_image');

		$the_music_project_section_main_content = get_field('the_music_project_section_main_content');

		$the_music_project_section_heading_2 = get_field('the_music_project_section_heading_2');

		$the_music_project_section_heading_2_content = get_field('the_music_project_section_heading_2_content');

		$the_music_project_section_heading_3 = get_field('the_music_project_section_heading_3');

		$the_music_project_section_heading_3_content = get_field('the_music_project_section_heading_3_content');

		$dear_children_sincerely_section_main_content = get_field('dear_children_sincerely_section_main_content');

		$dear_children_sincerely_section_main_image = get_field('dear_children_sincerely_section_main_image');

		$dear_children_sincerely_section_image_2 = get_field('dear_children_sincerely_section_image_2');

		$dear_children_sincerely_section_image_3 = get_field('dear_children_sincerely_section_image_3');

		$author_section_image = get_field('author_section_image');

		$author_section_content = get_field('author_section_content');

	endwhile; // End of the loop.

?>


	<div class="other-initiatives-page">
		
		<div class="container-fluid banner-sec">
			
			<div class="row">
				
				<div class="col-sm-12 logo">
					
					<a href="<?php echo site_url(); ?>">
						
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-2.png" alt="">

					</a>

				</div>

				<div class="col-sm-12 banner no-padding">
					<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
					
					<img class="main-imgs" src="<?php echo $page_banner_image; ?>" alt="">

				</div>

			</div>

		</div>

		<div class="container-fluid section-2"> 

			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> The Music Project  </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 no-padding">
					
					<div class="col-sm-6 sec-img no-padding">
						
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/other_inti_sec_1_img.jpg" alt="">

					</div>

					<div class="col-sm-6">
						
						<div class="content-1">

							<div class="content-para-1">
								
								<p> <?php echo $the_music_project_section_main_content; ?> </p>

							</div>

							<div class="content-para-2">
								
								<h3> <?php echo $the_music_project_section_heading_2; ?> </h3>

								<p> <?php echo $the_music_project_section_heading_2_content; ?> </p>

							</div>

							<div class="content-para-3">
								
								<h3> <?php echo $the_music_project_section_heading_3; ?></h3>

								<p> <?php echo $the_music_project_section_heading_3_content; ?> </p>

							</div>
							
							

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="container-fluid section-3">
			
			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> Dear Children Sincerely<br> - Stages Theatre Group  </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 no-padding">
					
					<div class="col-sm-6 content-sec">

						<?php echo $dear_children_sincerely_section_main_content; ?>

					</div>

					<div class="col-sm-6 no-padding">
						
						<div class="imglist">
							
							<div class="col-sm-12 main-img no-padding">

								<img src="<?php echo $dear_children_sincerely_section_main_image; ?>" />

							</div>

							<div class="col-sm-6 all-imgs set-img-padding-left">

								<img src="<?php echo $dear_children_sincerely_section_image_2; ?>" />

							</div>

							<div class="col-sm-6 all-imgs set-img-padding-right">

								<img src="<?php echo $dear_children_sincerely_section_image_3; ?>" />
		
							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="container-fluid section-4">

			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1>Author Lesley Hazleton<br> at the Galle Literary Festival </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 no-padding">
					
					<div class="col-sm-4 sec-img no-padding">
						
						<img src="<?php echo $author_section_image; ?>" alt="">

					</div>

					<div class="col-sm-8">
						
						<div class="content-sec">

							<?php echo $author_section_content; ?>
							
						</div>

					</div>

				</div>

			</div>
			
		</div>

	</div>


 <?php get_footer(); ?>