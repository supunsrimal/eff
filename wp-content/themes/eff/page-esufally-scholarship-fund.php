<?php
/**
 * Template Name: esufally scholarship fund
 *
 **/

	get_header(); 

		while ( have_posts() ) : the_post(); 

			//get page default values 

			$page_banner_image = get_field('page_banner_image');

			$banner_caption = get_field('banner_caption');

			$banner_content = get_field('banner_content');

			$the_initiative_section_content = get_field('the_initiative_section_content');

			$the_initiative_section_image = get_field('the_initiative_section_image');

		endwhile; // End of the loop.

?>

<div class="esufally-scholarship-fund-page">

	<div class="container-fluid banner-sec">
		
		<div class="row">
			
			<div class="col-sm-12 logo">
				
				<a href="<?php echo site_url(); ?>">
					
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-2.png" alt="">

				</a>

			</div>

			<div class="col-sm-12 banner no-padding">
				<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
				
				<img class="main-imgs" src="<?php echo $page_banner_image; ?>" alt="">

				<div class="banner-caption">

					<h1> <?php echo $banner_caption; ?> </h1>

					<p> <?php echo $banner_content; ?> </p>

				</div>

			</div>

		</div>

	</div>

	<div class="container-fluid section-2">
		
		<div class="row">

			<div class="col-sm-12">

			  <div class="col-sm-12 content-caption-box">

			    <div class="col-sm-12 outer">

			      <div class="col-sm-12">
			        
			        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

			      </div>

			      <div class="inner col-sm-10">

			        <h1> The Initiative </h1>

			        <hr>

			      </div>

			    </div>

			  </div>

			</div>

			<div class="col-sm-12">
				
				<div class="col-sm-6 content-sec">
					
					<?php echo $the_initiative_section_content; ?>

				</div>

				<div class="col-sm-6 img-sec">
					
					<img src="<?php echo $the_initiative_section_image; ?>" alt="">

				</div>

			</div>

		</div>

	</div>

	<div id="tut" class="container-fluid section-3">
		
		<div class="row">

			<div class="col-sm-12">

			  <div class="col-sm-12 content-caption-box">

			    <div class="col-sm-12 outer">

			      <div class="col-sm-12">
			        
			        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

			      </div>

			      <div class="inner col-sm-10">

			        <h1> TRUSTEES </h1>	

			        <hr>

			      </div>

			    </div>

			  </div>

			</div>

			<div class="col-sm-12 content-sec">

				<?php 

				    //wordpress query to get posts from post type esf_schol_fund_trust
				                
				    $args = array('posts_per_page' => 100,'post_type' => 'esf_schol_fund_trust');

				    $the_query_main = new WP_Query($args);

				    while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

				    $content_title = get_the_title();

				    $contents = get_the_content();

				?>
				
				<div class="col-sm-4 trustees">

					<div class="col-sm-12 trustees-title">
						
						<p> <?php echo $content_title; ?> </p>

					</div>

					<div class="col-sm-12 trustees-content">
						
						<div class="text short"></div>

						<div class="text full">

							<p> <?php echo $contents; ?> </p>
							
						</div>

					</div>

					<div class="col-sm-12 read-more">
						
						<hr>

					 	<p>read more</p>

					 	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/read-more-arr-down.png" alt="">

					</div>

				</div>

				<?php endwhile; //end loop ?>

			</div>

		</div>

	</div>
	
</div>


 <?php get_footer(); ?>


 <script>
   $(document).ready(function(){ 
   	   var img_url = '<?= get_bloginfo("stylesheet_directory"); ?>';    
       var maxChars = 186;
       var ellipsis = "...";
       $(".trustees").each(function() {
           var text = $(this).find(".text.full").text();
           var html = $(this).find(".text.full").html();        
           if(text.length > maxChars)
           {            
               var shortHtml = html.substring(0, maxChars - 3) + "<span class='ellipsis'>" + ellipsis + "</span>";
               $(this).find(".text.short").html(shortHtml);            
           }
       });
       $(".read-more").click(function(){        
           var readMoreText = "readmore";
           var readLessText = "readless";        
           var $shortElem = $(this).parent().find(".text.short");
           var $fullElem = $(this).parent().find(".text.full");        
           
           if($shortElem.is(":visible"))
           {           
               $shortElem.hide('8000');
               $fullElem.show('8000');
               $(this).parent().find("img").attr("src",img_url + "/images/read-more-arr-up.png");
               $(this).parent().find(".read-more p").text("");
               
           }
           else
           {
               $shortElem.show('8000');
               $fullElem.hide('8000');
               $(this).parent().find("img").attr("src",img_url + "/images/read-more-arr-down.png");
               $(this).parent().find(".read-more p").text(readMoreText);
           }       
       });
   });
 </script>