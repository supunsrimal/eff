<?php

/**
 * Template Name: article inner
 *
 **/

get_header(); 
  
?>

<style>
	#nav-toggle span{
		background: #071689;
	}
</style>
<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
<div class="artical-archive-all">

	<div class="container-fluid logo-sec-container">

		<div class="row">
			
			<div class="col-sm-12 logo-sec">
				
				<a href="<?php echo site_url(); ?>">
					
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-3.png" alt="">

				</a>

			</div>

		</div>
		
	</div>

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-sm-2 article-sidebar-wrapper">

				<div class="col-sm-3"></div>
				
				<?php get_sidebar(); ?>

			</div>

			<div class="col-sm-10">

				<div class="col-sm-12 all-articales single-article">

					<?php 

	   					while ( have_posts() ) : the_post();

	   					$article_heading = get_the_title();

	   					$article_intro_text = get_field('article_intro_text');

	   					$article_description = get_field('article_description');

	   					$article_banner_image = get_field('article_banner_image');

	   					$article_date = get_the_date('l jS F Y');

	   				?>
					
						<div class="col-sm-12 full-article">
							
							<div class="col-sm-12 article-img no-padding">
	
								<img src="<?php echo $article_banner_image; ?>" alt="">

							</div>

							<div class="col-sm-12 article-heading no-padding">
							
								<h2> <?php echo $article_heading; ?> </h2>
								
							</div>

							<div class="col-sm-12 article-date no-padding">
							
								<p> <?php echo $article_date; ?> </p>
								
							</div>

							<div class="col-sm-12 article-content no-padding">

								<?php echo $article_description; ?>
								
							</div>

						</div>

					<?php endwhile; ?>

				</div>

			</div>

		</div>

	</div>


<?php wp_reset_postdata();?>
	
</div>

<?php get_footer();  ?>

<script>

	$('document').ready(function() {

		if (($(window).width() > 480)){

			var document_height = $(document).height();

			var top_div_height = $('.logo-sec-container').height();

			var footer_height = $('.footer-section').height();

			var final_height = document_height-top_div_height-footer_height;

			$(".article-sidebar-wrapper").height(final_height);

		}

	});
</script>


