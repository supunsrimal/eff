<?php
/**
 * Template Name: index page
 *
 **/

get_header(); 

$featured_article_post_id = '';

?>

      <div class="home">
        
          <div class="container-fluid">

            <div class="row banner-slider">

              <div class="col-sm-12 no-padding">
                <?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
                <div class="scroll-down">
                  <a href="#feature-art"><span></span>Scroll Down</a>
                </div>
                
                <!-- Swiper -->
                <div class="swiper-container s1">

                  <div class="swiper-wrapper">

                  <?php 

                      //wordpress query to get posts from post type home_slider
                                  
                      $args = array('posts_per_page' => 100,'post_type' => 'home_slider');

                      $the_query_main = new WP_Query($args);

                      while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

                      $banner_slide_image = get_field('banner_slide_image');

                      $quote = get_field('quote');

                      $author_age = get_field('author_&_age');

                  ?>

                    <div class="swiper-slide">
                           
                      <img class="main-imgs" src="<?php echo $banner_slide_image; ?>" alt="">

                      <div class="col-sm-12 banner-caption-box">
                        
                        <div class="outer">
                          
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

                          <div class="inner">
                            
                            <h2> <?php echo $quote; ?> </h2>

                            <h3> <?php echo $author_age; ?> </h3>

                          </div>

                        </div>

                      </div>

                    </div>

                  <?php endwhile; //end loop ?>

                  </div>

                </div>
                <!-- Swiper End -->

              </div>

            </div>

        </div>

        <div id="feature-art" class="container-fluid section-1">

          <?php 
              //get Featured Article
              $the_query = new WP_Query( array(
                  'post_type' => 'home_article_slider',
                  'posts_per_page' => 1,
                  'tax_query' => array(
                      array (
                          'taxonomy' => 'article_types',
                          'field' => 'slug',
                          'terms' => 'featured_article',
                      )
                  ),
              ) );

              while ( $the_query->have_posts() ) : $the_query->the_post();

                $article_heading = get_the_title();

                $article_type = get_field('article_type');

                $article_description = get_field('article_description');

                $home_page_slider_article_image = get_field('home_page_slider_article_image');

                $featured_article_post_id = get_the_ID();

              endwhile; //end loop

          ?>
          
          <div class="row">
            
            <div class="col-sm-8">
              
              <div class="col-sm-12">

                <div class="col-sm-12 content-caption-box">

                  <div class="col-sm-12 outer">

                    <div class="col-sm-12">
                      
                      <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

                    </div>

                    <div class="inner col-sm-10">

                      <h2> <?php echo $article_type; ?> </h2>

                      <h1> <?php echo $article_heading; ?> </h1>  

                      <hr>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-sm-12 all_artl">
                
                <div class="page-content">

                  <div class="text short"></div>

                  <div class="text full">
                  
                    <?php echo $article_description; ?>

                  </div>

                  <p>

                    <a class="read-more" href="<?php the_permalink(); ?>">Read More</a>

                  </p>

                </div>

              </div>

            </div>

            <div class="col-sm-4 img-sec no-padding">

              <img src="<?php echo $home_page_slider_article_image; ?>" alt="">

            </div>

          </div>

        </div>

        <div class="container-fluid section-2">
          
          <div class="row">
            
            <div class="col-sm-12 no-padding">
              
              <!-- Swiper -->
              <div class="swiper-container s2">

                <div class="swiper-wrapper">

                  <?php 

                      //wordpress query to get posts from post type home_article_slider
                                  
                      $args = array('post__not_in' => array($featured_article_post_id),'posts_per_page' => 100,'post_type' => 'home_article_slider');

                      $the_query_main = new WP_Query($args);

                      while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

                      $article_heading = get_the_title();

                      $article_type = get_field('article_type');

                      $article_description = get_field('article_description');

                      $home_page_slider_article_image = get_field('home_page_slider_article_image');

                  ?>

                  <div class="swiper-slide">
                         
                    <div class="col-sm-4 img-sec no-padding" id="home-img-space">
                      
                      <img src="<?php echo $home_page_slider_article_image; ?>" alt="">

                    </div>

                    <div class="col-sm-8">
                      
                      <div class="col-sm-12">
                        
                        <div class="col-sm-12 content-caption-box">

                          <div class="col-sm-12 outer">

                            <div class="col-sm-12">
                              
                              <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

                            </div>

                            <div class="inner col-sm-10">

                              <h2> <?php echo $article_type; ?> </h2>

                              <h1> <?php echo $article_heading; ?> </h1>  

                              <hr>

                            </div>

                          </div>

                        </div>

                      </div>

                      <div class="col-sm-12 all_artl">
                
                        <div class="page-content">

                          <div class="text short"></div>

                          <div class="text full">
                          
                            <?php echo $article_description; ?>

                          </div>

                          <p>

                            <a class="read-more" href="<?php echo get_permalink(); ?>">Read More</a>

                          </p>

                        </div>

                    </div>

                    </div>

                  </div>

                  <?php endwhile; //end loop ?>

                </div>
                <?php
                if(wp_count_posts('home_article_slider')->publish > 2){ ?>
                    <!-- Add Arrows -->
                    <div class="col-sm-12 home-bottom-button">
                      <div class="swiper-button-next"></div>
    
                      <div class="swiper-button-prev"></div>
                    </div>

                <?php } ?>
              </div>
              <!-- Swiper End -->

            </div>

          </div>

        </div>

      </div>

 <?php get_footer(); ?>

 <!-- Initialize Swiper -->
 <script>

   var swiper_1 = new Swiper('.s1', {
      speed: 1000,
     autoplay: {

         delay: 5500,

     },

   });

   swiper_1.autoplay.start();

 </script>

 <script>
   
   var swiper_2 = new Swiper('.s2', {
         navigation: {
           nextEl: '.swiper-button-next',
           prevEl: '.swiper-button-prev',
         },
    });

 </script>

 <script>
   $(document).ready(function(){ 
       var img_url = '<?= get_bloginfo("stylesheet_directory"); ?>'; 
       var maxchr2 = 340;
       var ellipsis = "...";
    
       $(".all_artl").each(function() {
           var text = $(this).find(".text.full").text();
           var html = $(this).find(".text.full").html();        
           if(text.length > maxchr2)
           {            
               var shortHtml = html.substring(0, maxchr2 - 3) + "<span class='ellipsis'>" + ellipsis + "</span>";
               $(this).find(".text.short").html(shortHtml);            
           }
       });

   });
 </script>
 <script>
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
 </script>

  </body>
</html>