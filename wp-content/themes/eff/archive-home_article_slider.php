<?php


get_header(); 

$month_full_name = '';
  
?>

<style>
	#nav-toggle span{
		background: #071689;
	}
</style>
<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
<div class="artical-inner-pages"> 

	<div class="container-fluid logo-sec-container">

		<div class="row">
			
			<div class="col-sm-12 logo-sec">
				
				<a href="<?php echo site_url(); ?>">
					
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-3.png" alt="">

				</a>

			</div>

		</div>
		
	</div>

	<div class="container-fluid">

		<div class="row">

			<div class="col-sm-2 article-sidebar-wrapper">

				<div class="col-sm-3 article-inner-back-btn">

					<?php 

						$direct_page = get_post(38);

					?>
					
					<a href="<?php echo get_permalink($direct_page->ID); ?>">

						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/article-inner-back.png" alt="">

					</a>

				</div>
				
				<?php get_sidebar(); ?>

			</div>

			<div class="col-sm-10">

				<div class="col-sm-12 article-pagination">

					<?php
					    global $wp_query;
					    $big = 999999999; // need an unlikely integer
					    $args = array(
					        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					        'format' => '?page=%#%',
					        'total' => $wp_query->max_num_pages,
					        'current' => max( 1, get_query_var( 'paged') ),
					        'show_all' => false,
					        'end_size' => 3,
					        'mid_size' => 2,
					        'prev_next' => True,
					        'prev_text' => ('<img class="gal-slider-pre" src="http://localhost/EFF/wp-content/themes/eff/images/slider-pre.png" /> PREVIOUS '),
					        'next_text' => ('Next <img class="gal-slider-pre" src="http://localhost/EFF/wp-content/themes/eff/images/slider-next.png" />'),
					        'type' => 'plain',
					        );
					    echo paginate_links($args);
					?>

				</div>

				<?php 

					if ( have_posts() ) : 

					while ( have_posts() ) : the_post();

					$article_heading = get_the_title();

					$article_intro_text = get_field('article_intro_text');

					$article_description = get_field('article_description');

					$article_thumb_image = get_field('article_thumb_image');

					$article_date = get_the_date('l jS F Y');

					$month_full_name = get_the_date('F');

				?>

					<div class="col-sm-12 no-padding all-articales">
						
						<div class="col-sm-12 full-article">
							
							<div class="col-sm-12 article-img no-padding">
							
								<img src="<?php echo $article_thumb_image; ?>" alt="">

							</div>

							<div class="col-sm-12 article-heading no-padding">
							
								<h2> <?php echo $article_heading; ?> </h2>
								
							</div>

							<div class="col-sm-12 article-date no-padding">
							
								<p> <?php echo $article_date; ?> </p>
								
							</div>

							<div class="col-sm-12 article-content no-padding">
								
								<div class="text short"></div>

								<div class="text full">
								
									<?php echo $article_description; ?>

								</div>
								
							</div>

							<div class="col-sm-12 read-more no-padding">

							 	<a href="<?php the_permalink(); ?>">read more</a>

							 	<hr>

							</div>

						</div>

					</div>

				<?php endwhile; ?>

				<div class="col-sm-12 article-pagination">

					<?php
					    global $wp_query;
					    $big = 999999999; // need an unlikely integer
					    $args = array(
					        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					        'format' => '?page=%#%',
					        'total' => $wp_query->max_num_pages,
					        'current' => max( 1, get_query_var( 'paged') ),
					        'show_all' => false,
					        'end_size' => 3,
					        'mid_size' => 2,
					        'prev_next' => True,
					        'prev_text' => ('<img class="gal-slider-pre" src="http://localhost/EFF/wp-content/themes/eff/images/slider-pre.png" /> PREVIOUS '),
					        'next_text' => ('Next <img class="gal-slider-pre" src="http://localhost/EFF/wp-content/themes/eff/images/slider-next.png" />'),
					        'type' => 'plain',
					        );
					    echo paginate_links($args);
					?>

				</div>

			</div>

		</div>

	</div>

</div>

<?php

endif;
?>


<?php get_footer();  ?>

<script>

	$('document').ready(function() {

		if (($(window).width() > 480)){

			var document_height = $(document).height();

			var top_div_height = $('.logo-sec-container').height();

			var footer_height = $('.footer-section').height();

			var final_height = document_height-top_div_height-footer_height;

			$(".article-sidebar-wrapper").height(final_height+200);

		}

	});
</script>

<script>
	var url = window.location.href; 

	$('.article-side-bar li').each(function(){
		var list_url = $(this).find('a').attr('href');

		if(list_url === url){
			$(this).addClass('active');
		}
	    /*if( $(this).find('a').text() == '<?php echo $month_full_name; ?>' ) $(this).addClass('active');*/
	    
	});
</script>

<script>
  $(document).ready(function(){    
      var maxChars = 457;
      var ellipsis = "...";
      $(".full-article").each(function() {
          var text = $(this).find(".text.full").text();
          var html = $(this).find(".text.full").html();        
          if(text.length > maxChars)
          {            
              var shortHtml = html.substring(0, maxChars - 3) + "<span class='ellipsis'>" + ellipsis + "</span>";
              $(this).find(".text.short").html(shortHtml);            
          }
      });

  });
</script>

