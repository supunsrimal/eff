<?php
/**
 * Template Name: social cohesion through music
 *
 **/
	
	get_header(); 

		$article_inner_page = get_post(38);

		while ( have_posts() ) : the_post(); 

			//get page default values 

			$page_banner_image = get_field('page_banner_image');

			$banner_caption = get_field('banner_caption');

			$banner_content = get_field('banner_content');

			$music_for_social_cohesion_section_content = get_field('music_for_social_cohesion_section_content');

			$why_music_section_content = get_field('why_music_section_content');

			$music_matters_communities_section_image = get_field('music_matters_communities_section_image');

			$music_matters_communities_section_content = get_field('music_matters_communities_section_content');

			$volunteer_section_cover_image = get_field('volunteer_section_cover_image');

			$volunteer_section_mobile_cover_image = get_field('volunteer_section_mobile_cover_image');

			$volunteer_section_content = get_field('volunteer_section_content');

		endwhile; // End of the loop.

?>


	<div class="social-cohesion-page">
		
		<div class="container-fluid banner-sec">
			
			<div class="row">
				
				<div class="col-sm-12 logo">
					
					<a href="<?php echo site_url(); ?>">
						
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-2.png" alt="">

					</a>

				</div>

				<div class="col-sm-12 banner no-padding">
					<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
					
					<img class="main-imgs" src="<?php echo $page_banner_image; ?>" alt="">

					<div class="banner-caption">

						<h1> <?php echo $banner_caption; ?> </h1>

						<p> <?php echo $banner_content; ?> </p>

					</div>

				</div>

			</div>

		</div>

		<div class="container-fluid section-2">

			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> Music for Social Cohesion </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 content-sec">
					
					<?php echo $music_for_social_cohesion_section_content; ?>

				</div>

				<div class="col-sm-12 video-sec">

					<!-- Swiper -->
					 <div class="swiper-container">
					   <div class="swiper-wrapper">

					   	<?php 

					   		$args = array('posts_per_page' => 100,'post_type' => 'music_soc_coh_vid_sl');

					   		$the_query_main = new WP_Query($args);

					   		while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

					   		$youtube_video_url = get_field('youtube_video_url');

					   		//set YouTube video IDS

					   		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $youtube_video_url, $matches);


					   	?>

					     <div class="swiper-slide">
					     	
					     	<iframe id="video-player" width="100%" height="550" src="https://www.youtube.com/embed/<?php echo $matches[1]; ?>?list=RDMM<?php echo $matches[1]; ?>" frameborder="0" gesture="media" allowfullscreen></iframe>

					     </div>

					     <?php endwhile; //end loop ?>

					   </div>					   

					 </div>

					 <!-- Add Arrows -->
					     <div class="swiper-button-next"></div>
					     <div class="swiper-button-prev"></div>

					 <!-- Add Pagination -->
					 <div class="swiper-pagination"></div>
					
				</div>

			</div>
			
		</div>

		<div id="why_muc" class="container-fluid section-3">

			<div class="row">
				
				<div class="col-sm-12 content-sec-1">
					
					<h2> WHY MUSIC? </h2>

					<?php echo $why_music_section_content; ?>

				</div>

				<div class="col-sm-12 content-sec-2">

					<?php 

						$args = array('posts_per_page' => 100,'post_type' => 'msc_why_music_cmp');

						$the_query_main = new WP_Query($args);

						while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

						$cmt_title = get_the_title();

						$cmt_description = get_the_content();

					?>
					
						<div class="col-sm-4 content-wrapper">

							<div class="col-sm-12 title">

								<h2> <?php echo $cmt_title; ?> </h2>

							</div>

							<div class="col-sm-12 description">
								
								<p> <?php echo $cmt_description; ?> </p>

							</div>

						</div>

					<?php endwhile; //end loop ?>

				</div>

			</div>
			
		</div>

		<div id="mus_mat_com" class="container-fluid section-4">

			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> Musicmatters Communities </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 content-sec">
					
					<div class="col-sm-6 img-sec">
						
						<img src="<?php echo $music_matters_communities_section_image; ?>" alt="">

					</div>

					<div class="col-sm-5 description">
						
						<?php echo $music_matters_communities_section_content; ?>

					</div>

				</div>

			</div>
			
		</div>

		<div class="container-fluid section-5">

			<div class="row">
				
				<div class="col-sm-12 img-sec">

					<div class="col-sm-4 img-description">

						<h1>Volunteer!</h1>

						<?php echo $volunteer_section_content; ?>
						
					</div>

					<img src="<?php echo $volunteer_section_cover_image; ?>" alt="" class="hidden-xs">

					<img src="<?php echo $volunteer_section_mobile_cover_image; ?>" alt="" class="hidden-lg hidden-sm hidden-md">

				</div>

			</div>
			
		</div>

		<div id="adv_board" class="container-fluid section-6">
			
			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> Advisory Board </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 content-sec">

					<?php 

						$args = array('posts_per_page' => 100,'post_type' => 'msc_advisory_board');

						$the_query_main = new WP_Query($args);
						$i = 0;
						$post_count = $the_query_main->post_count;

						while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

						$adv_title = get_the_title();

						$adv_description = get_the_content();
						$i++;
						$m = $i%3 ;
						if($m == 1 ){
							echo '<div class="col-sm-12">';
						}

					?>
					
						<div class="col-sm-4 advisers">

							<div class="col-sm-12 advisers-title">
								
								<p> <?php echo $adv_title; ?> </p>

							</div>

							<div class="col-sm-12 advisers-content">
								
								<div class="text short"></div>

								<div class="text full">

									<p> <?php echo $adv_description; ?> </p>
									
								</div>

							</div>

							<div class="col-sm-12 read-more">
								
								<hr>

							 	<p>read more</p>

							 	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/read-more-arr-down.png" alt="">

							</div>

						</div>

					<?php
						if($m == 0 || $i == $post_count ){
							echo '</div>';
						}

					 endwhile; //end loop ?>

				</div>

			</div>

		</div>

		<div id="meet_muc" class="container-fluid section-7">
			
			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> MEET THE MUSICIANS </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>

				<div class="col-sm-12 featured-article-sec-wrapper">

					<div class="col-sm-10 featured-article-sec">
						
						<?php 
						    //get Featured Article
						    $the_query = new WP_Query( array(
						        'post_type' => 'home_article_slider',
						        'posts_per_page' => 1,
						        'tax_query' => array(
						            array (
						                'taxonomy' => 'article_types',
						                'field' => 'slug',
						                'terms' => 'featured_article',
						            )
						        ),
						    ) );

						    while ( $the_query->have_posts() ) : $the_query->the_post();

						      $article_sub_heading = get_field('article_sub_heading');

						      $article_description = get_field('article_description');

						      $article_date = get_the_date('l jS F Y');

						    endwhile; //end loop

						?>

						<div class="title">
							
							<h2> <?php echo $article_sub_heading; ?> </h2>

						</div>

						<div class="date">
							
							<p> <?php echo $article_date; ?> </p>

						</div>

						<div class="description">
							
							<div class="text short"></div>

							<div class="text full">
							
								<?php echo $article_description; ?>

							</div>

						</div>

					</div>					
						
					<div class="archive-btn">
						
						<a href="<?php echo get_permalink($article_inner_page->ID); ?>"> archives </a>

					</div>

				</div>

			</div>

		</div>

		<div id="gal" class="container-fluid section-8">
			
			<div class="row">

				<div class="col-sm-12">

				  <div class="col-sm-12 content-caption-box">

				    <div class="col-sm-12 outer">

				      <div class="col-sm-12">
				        
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/caption-box.png" alt="">

				      </div>

				      <div class="inner col-sm-10">

				        <h1> PROJECT GALLERY </h1>

				        <hr>

				      </div>

				    </div>

				  </div>

				</div>
				
				<div class="col-sm-12 gallery-sec">
					
					<div class="item">

					    <div class="clearfix">

					        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">

					        	<?php 

					        		$args = array('posts_per_page' => 100,'post_type' => 'msc_project_gallery');

					        		$the_query_main = new WP_Query($args);

					        		while ( $the_query_main->have_posts() ) : $the_query_main->the_post();

					        		$gallery_image = get_field('gallery_image');

					        		$gallery_thumbnail_image = get_field('gallery_thumbnail_image');

					        	?>

						            <li data-thumb="<?php echo $gallery_thumbnail_image; ?>"> 

						                <img src="<?php echo $gallery_image; ?>" />

						            </li>

					            <?php endwhile; //end loop ?>
					                         					                    
					        </ul>

					    </div>

					</div>

				</div>

			</div>

		</div>

	</div>


<?php get_footer(); ?>

<script src="<?php bloginfo('stylesheet_directory'); ?>/light-slider/js/lightslider.js"></script>

<!-- Initialize Swiper -->
<script>

  var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
  });

</script>

 <script>
   $(document).ready(function(){ 
   	   var img_url = '<?= get_bloginfo("stylesheet_directory"); ?>'; 
       var maxChars = 186;
       var maxchr2 = 468;
       var ellipsis = "...";
       $(".advisers").each(function() {
           var text = $(this).find(".text.full").text();
           var html = $(this).find(".text.full").html();        
           if(text.length > maxChars)
           {            
               var shortHtml = html.substring(0, maxChars - 3) + "<span class='ellipsis'>" + ellipsis + "</span>";
               $(this).find(".text.short").html(shortHtml);            
           }
       });

       $(".featured-article-sec").each(function() {
           var text = $(this).find(".text.full").text();
           var html = $(this).find(".text.full").html();        
           if(text.length > maxchr2)
           {            
               var shortHtml = html.substring(0, maxchr2 - 3) + "<span class='ellipsis'>" + ellipsis + "</span>";
               $(this).find(".text.short").html(shortHtml);            
           }
       });

       $(".read-more").click(function(){        
           var readMoreText = "readmore";
           var readLessText = "readless";        
           var $shortElem = $(this).parent().find(".text.short");
           var $fullElem = $(this).parent().find(".text.full"); 

 

           if($shortElem.is(":visible"))
           {   

       	    	$('.advisers-content .short').show('8000');      
       			$('.advisers-content .full').hide('8000');

               	$shortElem.hide('8000');
               	$fullElem.show('8000');
               	$(this).parent().find("img").attr("src",img_url + "/images/read-more-arr-up.png");
               	$(this).parent().find(".read-more p").text("");
               
           }
           else
           {	
               $shortElem.show('8000');
               $fullElem.hide('8000');
               $(this).parent().find("img").attr("src",img_url + "/images/read-more-arr-down.png");
               $(this).parent().find(".read-more p").text(readMoreText);
           }       
       });
   });
 </script>

 <script>
     	 $(document).ready(function() {
 			
             $('#image-gallery').lightSlider({
                 gallery:true,
                 item:1,
                 thumbItem:4,
                 slideMargin: 20,
                 speed:500,
                 auto:false,
                 loop:false,
                 prevHtml: '<img class="gal-slider-pre" src="<?php bloginfo('stylesheet_directory'); ?>/images/slider-pre.png" /> <div class="gal-slider-pre-text">PREVIOUS</div>',
        		 nextHtml: '<div class="gal-slider-next-text">NEXT</div><img class="gal-slider-next" src="<?php bloginfo('stylesheet_directory'); ?>/images/slider-next.png" />',
                 onSliderLoad: function() {
                     $('#image-gallery').removeClass('cS-hidden');
                 }  
             });
 		});
     </script>