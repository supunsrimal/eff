<?php

/**
 * Template Name: articles
 *
 **/

get_header(); 
  
?>

<?php

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$all_articales = new WP_Query(array(

		    'post_type'=>'home_article_slider',

		    'posts_per_page' => 3,

		    'paged' => $paged,

		));

?>
<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
<div class="artical-archive-all">

	<div class="container-fluid logo-sec-container">

		<div class="row">
			
			<div class="col-sm-12 logo-sec">
				
				<a href="<?php echo site_url(); ?>">
					
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-3.png" alt="">

				</a>

			</div>

		</div>
		
	</div>

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-sm-2 article-sidebar-wrapper">

				<div class="col-sm-3"></div>
				
				<?php get_sidebar(); ?>

			</div>

			<div class="col-sm-10">
				
				<div class="col-sm-12 article-pagination">
					
					<?php

					$total_pages = $all_articales->max_num_pages;

					$img_url_pre = get_bloginfo('stylesheet_directory').'/images/slider-pre.png';

					$img_url_next = get_bloginfo('stylesheet_directory').'/images/slider-next.png';

					if ($total_pages > 1){

					    $current_page = max(1, get_query_var('paged'));

					    echo paginate_links(array(

					        'base'               => get_pagenum_link(1) . '%_%',

					        'format'             => '/page/%#%',

					        'total'              => $total_pages,

					        'current'            => $current_page,

					        'show_all'           => false,

					        'end_size'           => 1,

					        'mid_size'           => 2,

					        'prev_next'          => true,

					        'prev_text'          => ('<img class="gal-slider-pre" src="'.$img_url_pre.'" /> <div class="gal-slider-pre-text"><p>PREVIOUS</p></div>'),

					        'next_text'          => ('<div class="gal-slider-next-text"><p>NEXT</p></div><img class="gal-slider-next" src="'.$img_url_next.'" />'),

					        'type'               => 'html',

					        'add_args'           => false,

					    ));
					}

					?>

				</div>

				<div class="col-sm-12 all-articales">

					<?php 

						if($all_articales->have_posts()) :

	   						while($all_articales->have_posts())  : $all_articales->the_post();

	   						$article_heading = get_the_title();

	   						$article_intro_text = get_field('article_intro_text');

	   						$article_description = get_field('article_description');

	   						$article_thumb_image = get_field('article_thumb_image');

	   						$article_date = get_the_date('l jS F Y');

	   				?>
					
						<div class="col-sm-12 full-article">
							
							<div class="col-sm-12 article-img no-padding">
	
								<img src="<?php echo $article_thumb_image; ?>" alt="">

							</div>

							<div class="col-sm-12 article-heading no-padding">
							
								<h2> <?php echo $article_heading; ?> </h2>
								
							</div>

							<div class="col-sm-12 article-date no-padding">
							
								<p> <?php echo $article_date; ?> </p>
								
							</div>

							<div class="col-sm-12 article-content no-padding">

								<div class="text short"></div>

								<div class="text full">
								
									<?php echo $article_description; ?>

								</div>
								
							</div>

							<div class="col-sm-12 read-more no-padding">

							 	<a href="<?php the_permalink(); ?>">read more</a>

							 	<hr>

							</div>

						</div>

					<?php 

							endwhile; 

						endif;
					?>

				</div>

				<div class="col-sm-12 article-pagination">
					
					<?php

					$total_pages = $all_articales->max_num_pages;

					$img_url_pre = get_bloginfo('stylesheet_directory').'/images/slider-pre.png';

					$img_url_next = get_bloginfo('stylesheet_directory').'/images/slider-next.png';

					if ($total_pages > 1){

					    $current_page = max(1, get_query_var('paged'));

					    echo paginate_links(array(

					        'base'               => get_pagenum_link(1) . '%_%',

					        'format'             => '/page/%#%',

					        'total'              => $total_pages,

					        'current'            => $current_page,

					        'show_all'           => false,

					        'end_size'           => 1,

					        'mid_size'           => 2,

					        'prev_next'          => true,

					        'prev_text'          => ('<img class="gal-slider-pre" src="'.$img_url_pre.'" /> <div class="gal-slider-pre-text"><p>PREVIOUS</p></div>'),

					        'next_text'          => ('<div class="gal-slider-next-text"><p>NEXT</p></div><img class="gal-slider-next" src="'.$img_url_next.'" />'),

					        'type'               => 'html',

					        'add_args'           => false,

					    ));
					}

					?>

				</div>

			</div>

		</div>

	</div>


<?php wp_reset_postdata();?>
	
</div>

<?php get_footer();  ?>

<script>

	$('document').ready(function() {

		if (($(window).width() > 480)){

			var document_height = $(document).height();

			var top_div_height = $('.logo-sec-container').height();

			var footer_height = $('.footer-section').height();

			var final_height = document_height-top_div_height-footer_height;

			$(".article-sidebar-wrapper").height(final_height+200);

		}

	});
</script>

<script>
  $(document).ready(function(){    
      var maxChars = 457;
      var ellipsis = "...";
      $(".full-article").each(function() {
          var text = $(this).find(".text.full").text();
          var html = $(this).find(".text.full").html();        
          if(text.length > maxChars)
          {            
              var shortHtml = html.substring(0, maxChars - 3) + "<span class='ellipsis'>" + ellipsis + "</span>";
              $(this).find(".text.short").html(shortHtml);            
          }
      });

  });
</script>

