<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <?php wp_head(); ?>

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css" rel="stylesheet">
      
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/swiper-slider/css/swiper.min.css">

    <link rel="stylesheet"  href="<?php bloginfo('stylesheet_directory'); ?>/light-slider/css/lightslider.css"/>

    <!-- Custom CSS File -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css">

    <!-- xs CSS -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-screen-xs.css" />
    <!-- tab CSS -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-screen-tab.css" />
    <!-- sm CSS -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-screen-sm.css" />
    <!-- lg over 1600 CSS -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/media-screen-lg.css" /> 

  </head>
  <body>
    
    <div class="container-fluid">

        <div class="row">

          <?php

            $page_title = $wp_query->post->post_title; 

            if($page_title == 'Home') { ?>

            <div class="col-xs-12 col-sm-12 logo">

              <a href="<?php echo site_url(); ?>">

                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="">
                
              </a>

            </div>

            <?php } ?>

        </div>

        <div class="row">

          <div class="col-sm-12">
            
            <div id="wrap">

              <?php if($page_title == 'Article Inner'){ ?>

                <div id="nav-toggle" class="<?php echo 'set-nav-color-blue' ?>">

              <?php }else{?>

                <div id="nav-toggle" class="<?php echo 'set-nav-color-default' ?>">

              <?php }  ?>
              
                <span></span>
                <span></span>
                <span></span>
              
              </div>


              
              <!-- <nav id="side-nav">

               <?php
                          //  wp_nav_menu( array(
                          //      'menu'              => 'main_menu',
                          //      'theme_location'    => 'primary',
                          //      'depth'             => 2,
                          //      'container'         => 'div',
                          //      'menu_class'        => 'nav navbar-nav',
                          //      'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                          //      'walker'            => new WP_Bootstrap_Navwalker())
                          //  );
                ?>
              
              </nav> -->

            </div>

          </div>

        </div>

      </div>