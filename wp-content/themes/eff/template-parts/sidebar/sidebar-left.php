<nav id="side-nav">
<?php
           wp_nav_menu( array(
               'menu'              => 'main_menu',
               'theme_location'    => 'primary',
               'depth'             => 2,
               'container'         => 'div',
               'menu_class'        => 'nav navbar-nav',
               'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
               'walker'            => new WP_Bootstrap_Navwalker())
           );
?>
</nav>
