<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eff_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J0j<qv:4%9qUthVm(wV,-1tJE1<v,H6|DeF4~{1&s&PJcDb3>[#F)Z+k*vjO7Cf]');
define('SECURE_AUTH_KEY',  '^F&R):I8.=U#HLY|XNPyo z~AV?9z;*AA<Rlc6x$chd9K<#yQ&7[t=[5C8wE--=5');
define('LOGGED_IN_KEY',    'n`SFIipbjHl7SndPUW )>x(#cCjFt+5$)`j-*au*F%wr90G5ip7%.5C%l)6Rd&PF');
define('NONCE_KEY',        'h~x*okd83DNKWDS[`m1Te1k[(O32YdY$/?(btq[P>TfZ]mxF/Y0>4&?U4p`b` TM');
define('AUTH_SALT',        'Kje}Zwe}JdfKIyc4wu8{{6]Sd;jimR9}_ d Ts[LlHQDMM}WKr1(wR4yvk]aIM.l');
define('SECURE_AUTH_SALT', 'Dfe)s.0u~.zRWVIaYC|7H3:iG!]Vu9orDYs i>?`Py5<;nPDz>Q<nZra)-m~`(eD');
define('LOGGED_IN_SALT',   'dfZ2X!9obUU$&1qS|R8_Hk^gV#xz3 af~f?|%`H()2y`C)&JM%PJV$Tiwq3RfeF2');
define('NONCE_SALT',       '^Zfs_D_h<vxh3!P/_/tgKNKwvFYu5<@RE!5Hu2|9kGxG]>ZSaRN(=_ISPN(^>/wE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
